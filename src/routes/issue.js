const express = require("express");
const router = express.Router();
const { validateSchema } = require("../../middlewares/validation");
const authenticateMiddleware = require("../../middlewares/auth.js");
const logger = require("../logger");

const IssueService = require("../../services/issuesService");
const issueService = new IssueService();

router.get(
  "/issues/:issue_id",
  authenticateMiddleware(),
  async (req, res, next) => {
    try {
      const { issue_id } = req.params;
      const issueResult = await issueService.getIssueById(issue_id);
      res.json({ success: issueResult.success, issue: issueResult.issue });
    } catch (error) {
      logger.error(error);
      res.status(500).json({ error: { message: "Internal server error" } });
    }
  }
);

router.post(
  "/sync",
  [authenticateMiddleware(), validateSchema(issueSchema)],
  async (req, res, next) => {
    try {
      logger.info("From the route");
      const result = await issueService.syncWithLocalDB();
      res.json(result);
    } catch (error) {
      logger.error(error);
      res.status(500).json({ error: { message: "Internal server error" } });
    }
  }
);

router.put(
  "/issues/:issue_id",
  [authenticateMiddleware(), validateSchema(issueSchema)],
  async (req, res, next) => {
    try {
      const { issue_id } = req.params;
      const updatedIssueDetails = req.body;
      const updatedIssueResult = await issueService.updateIssueById(
        issue_id,
        updatedIssueDetails
      );
      res.json({
        success: updatedIssueResult.success,
        updatedIssue: updatedIssueResult.updatedIssue,
      });
    } catch (error) {
      logger.error(error);
      res.status(500).json({ error: { message: "Internal server error" } });
    }
  }
);

module.exports = router;
