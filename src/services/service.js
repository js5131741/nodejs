const axios = require("axios");
const Issue = require("../models/Issue");
const logger = require("../logger");

class IssueService {
  async syncWithLocalDB() {
    const owner = process.env.REPO_OWNER;
    const repo = process.env.REPO_NAME;
    const batchSize = 3;

    try {
      const response = await axios.get(
        `https://api.github.com/repos/${owner}/${repo}/issues`,
        {
          headers: {
            Authorization: `Bearer ${process.env.GITHUB_TOKEN}`,
          },
        }
      );

      const issues = response.data;
      logger.info(issues);

      for (let i = 0; i < issues.length; i += batchSize) {
        const batch = issues.slice(i, i + batchSize);
        const promises = batch.map(async (issueData) => {
          const validatedIssue = new Issue(issueData);
          await validatedIssue.validate();

          await Issue.findOneAndUpdate({ id: issueData.id }, issueData, {
            upsert: true,
          });
        });

        await Promise.all(promises);

        await new Promise((resolve) => setTimeout(resolve, 1000));
      }

      return { success: true, message: "Sync completed successfully" };
    } catch (error) {
      logger.error(error);
      throw new Error("Error syncing data");
    }
  }

  async getIssueById(issueId) {
    try {
      const issue = await Issue.findOne({ id: issueId });

      if (!issue) {
        throw new Error("Issue not found");
      }

      return { success: true, issue };
    } catch (error) {
      logger.error(error);
      throw new Error("Error retrieving issue");
    }
  }

  async updateIssueById(issueId, updatedIssueDetails) {
    try {
      const updatedIssue = await Issue.findOneAndUpdate(
        { id: issueId },
        updatedIssueDetails,
        {
          new: true,
        }
      );

      if (!updatedIssue) {
        throw new Error("Issue not found");
      }

      return { success: true, updatedIssue };
    } catch (error) {
      logger.error(error);
      throw new Error("Error updating issue");
    }
  }
}

module.exports = IssueService;
