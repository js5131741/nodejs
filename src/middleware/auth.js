const jwt = require("jsonwebtoken");
const passport = require("passport");
const authenticatemiddleware = () => (req, res, next) => {
  passport.authenticate("jwt", { session: false }, (err, user) => {
    if (err) {
      console.log("Error decoding token:", err);
      return res.status(403).json({ message: "Unauthorized: Invalid token" });
    }
    req.user = user;
    next();
  })(req, res, next);
};

module.exports = authenticatemiddleware;
