const { createLogger } = require("pino");

const logger = createLogger({ prettyPrint: true });

module.exports = logger;
